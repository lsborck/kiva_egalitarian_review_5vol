# KIVA_Egalitarian_Review_5Vol

Data from the review of published articles in KIVA: The Journal of Southwestern Archaeology and History.

All articles published in KIVA from Volume 80 through Volume 85 were examined and qualitatively coded for level of hierarchy and egalitarianism present in the culture area/period the researcher was working in. These were semi-quantitatvely expressed as 3 (extreme hierarchy), 2 (moderate hierarchy), 1 (low hierarchy), -1 (low egalitarian), -2 (moderate egalitarian), -3 (extreme egalitarian). For the Multiple correspondence analysis, the semi-quantitative numbers were transformed into the string variable. Low egalitarian/low hierarchy mean low on the scale of egalitarian/hierarchy, not low hierarchy with high egalitarian as an example.

Articles that were about artifact classes that cross multiple culture areas and times were not coded.